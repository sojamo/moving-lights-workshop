/*
   Moving Lights workshop
   Andreas Schlegel
   https://gitlab.com/sojamo/moving-lights-workshop
   
   Rainbow
   This sketch animates a number of single LEDs in rainbow colors.
   Make changes to variables prefixed with m
   Animation takes place for a short period of time followed by a pause (mPauseInMillis).
   After a number of cycles, the program pauses for a longer time (mSilentInMillis)
   Review function checkStatus and make changes where necessary.

   Make sure that the number of pixels (NUMPIXELS) is set correctly.
   
   After Internet 632
   Total Art Museum, http://totalmuseum.org/
   Seoul, 2019.
*/


#include <Adafruit_NeoPixel.h>

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN 5

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 24

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);


// add the HSV to RGB conversion fuction to the sketch
#include "hsv.h"


// a counter that increases for each loop iteration
unsigned long gFrameCount = 0;
int gCycles = 0;
const int gDelayInMillis = 20;
float gIntensity = 0;
float gNIntensity = 1;

//
// Make changes here
// 
int mStartPixel = 0;                     // the number of the first LED 
int mEndPixel = NUMPIXELS;               // the number of the last LED  
float mRainbowSpeed = 0.1;               // the speed of the rainbow moving
float mRainbowStretch = 110.20;          // the higher the number, the more noisy the rainbow
int mOverExposure = 0;                   // the higher the number the more white is added to a color  
int mMaxCycles = 8;                      // how many cycles to perform before going into silent mode
int mPauseInMillis = 2000;               // after each active mode, the light pauses for x milliseconds, this completes a cycle
int mSilentInMillis = 10000;             // after maxCycles is completed, the light pauses for x milliseconds
int mActiveInMillis = 2000;              // the light stays active for x milliseconds



void setup() {
  pixels.begin(); // This initializes the NeoPixel library
}


void loop() {

  // if gFrameCount has reached a certain maximum,
  // then fade out LEDs and pause

  if (gFrameCount >= mActiveInMillis/gDelayInMillis) {
    gNIntensity = 0;
  }

  gIntensity += (gNIntensity - gIntensity) * (gNIntensity == 1 ? 0.01 : 0.1);

  const float rainbowOffset = mRainbowStretch / mRainbowSpeed;
  for (int i = mStartPixel; i < mEndPixel; i++) {
    int h = 127 + round(sin((gFrameCount + (i * rainbowOffset)) * mRainbowSpeed ) * 127);
    h *= 6;
    pixels.setPixelColor(i, getPixelColorHsv(i, h, 255-mOverExposure, 255 * gIntensity));
  }



  pixels.show(); // This sends the updated pixel color to the hardware.
  delay(gDelayInMillis ); // Delay for a period of time (in milliseconds).
  gFrameCount++;

  checkStatus();
}

void checkStatus() {

  if (gNIntensity == 0 && gIntensity < 0.01) {
    // clearPixels();
    pixels.clear();
    pixels.show();

    // pause for a moment
    delay(mPauseInMillis);

    // now move on to the next cycle
    // and reset parameters
    gCycles++;
    if (gCycles == mMaxCycles) {
      // after 10 cycles, keep quiet for longer
      delay(mSilentInMillis);
      gCycles = 0;
    }

    gIntensity = 0;
    gFrameCount = 0;
    gNIntensity = 1;

    mRainbowSpeed = random(0, 10) * 0.025;

    // randomly select a section from
    // the LED-strip to animate.
    mEndPixel = random(4, 8);
    mStartPixel = random(0, NUMPIXELS - mEndPixel);
    mEndPixel += mStartPixel;
  }
}

