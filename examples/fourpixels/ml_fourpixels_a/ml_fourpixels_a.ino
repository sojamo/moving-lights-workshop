/*
   Moving Lights workshop
   Andreas Schlegel
   https://gitlab.com/sojamo/moving-lights-workshop
   
   Four Pixels
   A test sketch to get started with addressable LED strips.
   The test strip has 4 LEDs.

   Make sure that the number of pixels (NUMPIXELS) is set correctly.

   After Internet 632
   Total Art Museum, http://totalmuseum.org/
   Seoul, 2019.
*/


// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

#include <Adafruit_NeoPixel.h>

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN 5

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 4

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);


// a counter that increases for each loop iteration
unsigned long gFrameCount = 0;
const int gDelayInMillis = 20;
float gIntensity = 0;
float gNIntensity = 1;
int gCurrentIndex = 0;

//
// Make Changes here
//
int mStartPixel = 0;           // the number of the first LED 
int mEndPixel = NUMPIXELS;     // the number of the last LED
int mActiveInMillis = 1000;    // the light stays active for x milliseconds



void setup() {
  pixels.begin(); // This initializes the NeoPixel library
}


void loop() {
  if (gFrameCount >= mActiveInMillis/gDelayInMillis) {
    gFrameCount = 0;
    gCurrentIndex = gCurrentIndex + 1;          // increase gCurrentIndex by 1
    gCurrentIndex = gCurrentIndex % NUMPIXELS;  // use modulo to prevent gCurrentIndex become bigger than NUMPIXELS and instead go back to 0.
  }
  
  pixels.clear();
  float r = 255;
  float g = 255;
  float b = 255;  
  pixels.setPixelColor(gCurrentIndex, r, g, b);


  pixels.show(); // This sends the updated pixel color to the hardware.
  delay(gDelayInMillis); // Delay for a period of time (in milliseconds).
  gFrameCount++;

}

