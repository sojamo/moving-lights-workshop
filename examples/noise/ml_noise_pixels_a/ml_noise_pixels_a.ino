/*
   Moving Lights workshop
   Andreas Schlegel
   https://gitlab.com/sojamo/moving-lights-workshop
   
   Noise pixels
   This sketch animates a number of single LEDs randomly, and lights them up black or white.
   Make changes to variables prefixed with m
   Animation takes place for a short period of time followed by a pause (mPauseInMillis).
   After a number of cycles, the program pauses for a longer time (mSilentInMillis)
   Review function checkStatus and make changes where necessary.

   Make sure that the number of pixels (NUMPIXELS) is set correctly.

   After Internet 632
   Total Art Museum, http://totalmuseum.org/
   Seoul, 2019.
*/


// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

#include <Adafruit_NeoPixel.h>

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN 5

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 24

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);


// a counter that increases for each loop iteration
unsigned long gFrameCount = 0;
int gCycles = 0;
const int gDelayInMillis = 20;
float gIntensity = 0;
float gNIntensity = 1;


//
// Make Changes here
//
int mStartPixel = 0;           // the number of the first LED 
int mEndPixel = NUMPIXELS;     // the number of the last LED
int mMaxCycles = 10;           // how many cycles to perform before going into silent mode
int mPauseInMillis = 4000;     // after each active mode, the light pauses for x milliseconds, this completes a cycle
int mSilentInMillis = 10000;   // after maxCycles is completed, the light pauses for x milliseconds
int mActiveInMillis = 2000;    // the light stays active for x milliseconds



void setup() {
  pixels.begin(); // This initializes the NeoPixel library
}


void loop() {

  if (gFrameCount >= mActiveInMillis/gDelayInMillis) {
    gNIntensity = 0;
  }

  gIntensity += (gNIntensity - gIntensity) * (gNIntensity == 1 ? 0.05 : 0.1);

  for (int i = mStartPixel; i < mEndPixel; i++) {
    if (gNIntensity == 1) {

      // make changes here
      int val = gIntensity > 0.9 ? 64 : int( ((gFrameCount * 2 + i * random(0, 20)) % 255) * gIntensity);
      float r = val;
      float g = val;
      float b = val;
      pixels.setPixelColor(i, r, g, b);
    } else {
      int val = int(random(0, 255) * gIntensity);
      float r = val;
      float g = val;
      float b = val;
      pixels.setPixelColor(i, r, g, b);
    }
  }


  pixels.show(); // This sends the updated pixel color to the hardware.
  delay(gDelayInMillis); // Delay for a period of time (in milliseconds).
  gFrameCount++;

  checkStatus();

}


void checkStatus() {
  if (gIntensity < 0.01) {
    pixels.clear();
    pixels.show();

    delay(mPauseInMillis);

    gCycles++;
    if (gCycles == mMaxCycles) {
      delay(mSilentInMillis);
      gCycles = 0;
    }

    gFrameCount = 0;
    gIntensity = 0;
    gNIntensity = 1;

    // randomly select a section from
    // the LED-strip to animate.
    mEndPixel = random(3, 8);
    mStartPixel = random(0, NUMPIXELS - mEndPixel);
    mEndPixel += mStartPixel;

  }

}

