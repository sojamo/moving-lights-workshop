/*
   Moving Lights workshop
   Andreas Schlegel
   https://gitlab.com/sojamo/moving-lights-workshop
   
   Random pixel b
   Based on a timed interval, a small set of random leds will turn on.

   Make sure that the number of pixels (NUMPIXELS) is set correctly.

   After Internet 632
   Total Art Museum, http://totalmuseum.org/
   Seoul, 2019.
*/


// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

#include <Adafruit_NeoPixel.h>

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN 5

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 24

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);


// a counter that increases for each loop iteration
unsigned long gFrameCount = 0;
int gCycles = 0;
const int gDelayInMillis = 20;
float gIntensity = 0.5;
int gActivePixels[] = {0, 0, 0, 0};  // add or remove items from the array to change the amount of random LEDs lighting up periodically.
int mActiveInMillis = 2000;          // the light stays active for x milliseconds before it changes


void setup() {
  pixels.begin(); // This initializes the NeoPixel library
}


void loop() {
  
  if (gFrameCount >= mActiveInMillis / gDelayInMillis) {
    gFrameCount = 0;      // reset the frame counter
    pixels.clear();       // set all LEDs to black
    
    for(int i=0;i<sizeof(gActivePixels)/sizeof(int);i++) {
      gActivePixels[i] = random(0,NUMPIXELS);
      float val = 255 * gIntensity;
      float r = val;
      float g = val;
      float b = val;
      pixels.setPixelColor(gActivePixels[i], r, g, b);
    }
    pixels.show();        // This sends the updated pixel color to the hardware.
  }

  delay(gDelayInMillis);  // Delay for a period of time (in milliseconds).
  gFrameCount++;


}


