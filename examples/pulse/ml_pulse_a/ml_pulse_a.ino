/*
   Moving Lights workshop
   Andreas Schlegel
   https://gitlab.com/sojamo/moving-lights-workshop
   
   Pulse
   This sketch animates a number of single LEDs in a pulsing fashion.
   Make changes to variables to change speed (mPulseSpeed) and movement (mPulseOffset) of the pulsing light.

   Continuously pulses without pause.
   
   Make sure that the number of pixels (NUMPIXELS) is set correctly.
   
   After Internet 632
   Total Art Museum, http://totalmuseum.org/
   Seoul, 2019.
*/


// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

#include <Adafruit_NeoPixel.h>

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN 5

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 24

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);


// a counter that increases for each loop iteration
unsigned long gFrameCount = 0;
const int gDelayInMillis = 20;

// 
// Make changes here
// 
float mPulseFreq = 0.1;         // the frequency of the pulse
float mPulseOffset = 1.2;       // the movement of the pulse

  
void setup() {
  pixels.begin(); // This initializes the NeoPixel library
}

void loop() {

  pixels.clear();

  for (int i = 0; i < NUMPIXELS; i++) {
    float wave = sin((gFrameCount + (i * mPulseOffset)) * mPulseFreq); // will calculate a value between -1 and 1
    float val =  127 + wave * 127;  // map value to a value between 0 and 255
    val *= 0.25;                    // adjust brightness
    float r = val * 0.9;            // adjust intensity of the red color channel
    //r = val * 0.19 * i;             // adjust intensity of the red color channel
    float g = val * 0.9;            // adjust intensity of the green color channel
    float b = val * 0.25;           // adjust intensity of the blue color channel
    pixels.setPixelColor(i, pixels.Color(r,g,b)); // assign color to led-pixel
  }
  
  pixels.show(); // This sends the updated pixel color to the LED strip.

  delay(gDelayInMillis);
  gFrameCount++;

}


