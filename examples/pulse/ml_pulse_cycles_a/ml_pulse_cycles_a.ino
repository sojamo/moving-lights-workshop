
/*
   Moving Lights workshop
   Andreas Schlegel
   https://gitlab.com/sojamo/moving-lights-workshop
   
   Pulse cycles
   This sketch animates a number of single LEDs in a pulsing way for a short period of time followed by a pause.
   Make changes to variables prefixed with m.
   For example make changes to the speed (mPulseSpeed) and movement (mPulseOffset) of the pulsing light.
   Animation takes place for a short period of time followed by a pause (mPauseInMillis).
   After a number of cycles, the program pauses for a longer time (mSilentInMillis)
   Review function checkStatus and make changes where necessary.

   Make sure that the number of pixels (NUMPIXELS) is set correctly.

   After Internet 632
   Total Art Museum, http://totalmuseum.org/
   Seoul, 2019.
*/


#include <Adafruit_NeoPixel.h>

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN 5

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 24

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);


// a counter that increases for each loop iteration
unsigned long gFrameCount = 0;
unsigned long gCycles = 0;
const int gDelayInMillis = 20;
float gIntensity = 0;
float gNIntensity = 1;


int mStartPixel = 0;                  // the number of the first LED 
int mEndPixel = NUMPIXELS;            // the number of the last LED  
float mPulseFreq = 0.1;               // the frequency of the pulse
float mPulseOffset = 1.2;             // the movement of the pulse
int mMaxCycles = 10;                  // how many cycles to perform before going into silent mode
int mPauseInMillis = 3000;            // after each active mode, the light pauses for x milliseconds, this completes a cycle
int mSilentInMillis = 15000;          // after maxCycles is completed, the light pauses for x milliseconds
int mActiveInMillis = 2000;           // the light stays active for x milliseconds


void setup() {
  pixels.begin(); // This initializes the NeoPixel library
}


void loop() {

  if (gFrameCount >= mActiveInMillis/gDelayInMillis) {
    gNIntensity = 0;
  }

  gIntensity += (gNIntensity - gIntensity) * (gNIntensity == 1 ? 0.01 : 0.1);

  for (int i = mStartPixel; i < mEndPixel; i++) {
    float wave = cos((gFrameCount + (i * mPulseOffset)) * mPulseFreq);
    float val =  (127 + wave * 127) * gIntensity;
    float r = val * 0.1 * i;
    float g = val;
    float b = val * 0.25;
    pixels.setPixelColor(i, pixels.Color(r, g, b));
  }

  pixels.show(); // This sends the updated pixel color to the hardware.
  delay(gDelayInMillis ); // Delay for a period of time (in milliseconds).
  gFrameCount++;

  checkStatus();
}

void checkStatus() {
  if (gNIntensity == 0 && gIntensity < 0.01) {
    pixels.clear();
    pixels.show();

    // pause for a moment
    delay(mPauseInMillis);

    // now move on to the next cycle
    // and reset parameters
    gCycles++;
    if (gCycles == mMaxCycles) {
      delay(mSilentInMillis);
      gCycles = 0;
    }

    gIntensity = 0;
    gFrameCount = 0;
    gNIntensity = 1;

    mPulseOffset = random(0, 10);
    mPulseFreq = random(1, 10) * 0.02;


    // randomly select a section from
    // the LED-strip to animate.

    mEndPixel = random(4, 8);
    mStartPixel = random(0, NUMPIXELS - mEndPixel);
    mEndPixel += mStartPixel;
  }

}

