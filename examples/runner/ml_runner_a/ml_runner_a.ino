/*
   Moving Lights workshop
   Andreas Schlegel
   https://gitlab.com/sojamo/moving-lights-workshop
   
   Runner
   This sketch animates an LED trail running from one end to the other of an LEDstrip.
   Make changes to variables prefixed with m

   Make sure that the number of pixels (NUMPIXELS) is set correctly.

   After Internet 632
   Total Art Museum, http://totalmuseum.org/
   Seoul, 2019.
*/


// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

#include <Adafruit_NeoPixel.h>

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN 5

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 24

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);


// a counter that increases for each loop iteration
unsigned long gFrameCount = 0;
int gCycles = 0;
const int gDelayInMillis = 20;
float gIntensity = 0;
float gNIntensity = 1;

//
// Make Changes here
//
int mStartPixel = 0;           // the number of the first LED 
int mEndPixel = NUMPIXELS;     // the number of the last LED
int mTrailLength = 3;          // the length of the light trail, number must be bigger than 0
int mSlowDownByFactor = 2;     // the factor by which to slow down the light trail
int mMaxCycles = 8;            // how many cycles to perform before going into silent mode
int mPauseInMillis = 4000;     // after each active mode, the light pauses for x milliseconds, this completes a cycle
int mSilentInMillis = 10000;   // after maxCycles is completed, the light pauses for x milliseconds
int mActiveInMillis = 1000;    // the light stays active for x milliseconds


void setup() {
  pixels.begin(); // This initializes the NeoPixel library
}


void loop() {

  if (gFrameCount >= mActiveInMillis/gDelayInMillis) {
    gNIntensity = 0;
  }

  // fade in and fade out.
  gIntensity += (gNIntensity - gIntensity) * (gNIntensity == 1 ? 0.05 : 0.1);

    pixels.clear();

    int len = mEndPixel - mStartPixel;
    int n = int((gFrameCount / mSlowDownByFactor) % len);
    int steps = 255 / (mTrailLength + 1);
    int brightness = steps;
    for (int i = 0; i < mTrailLength; i++) {
      int val = brightness * gIntensity;
      float r = val;
      float g = val;
      float b = val;
      pixels.setPixelColor((n + i) % len, r, g, b);
      brightness += steps;
    }

  pixels.show(); // This sends the updated pixel color to the hardware.
  delay(gDelayInMillis); // Delay for a period of time (in milliseconds).
  gFrameCount++;

  checkStatus();

}
void checkStatus() {
  if (gIntensity < 0.01) {
    pixels.clear();
    pixels.show();

    delay(mPauseInMillis);

    gCycles++;
    if (gCycles == mMaxCycles) {
      delay(mSilentInMillis);
      gCycles = 0;
    }

    gFrameCount = 0;
    gIntensity = 0;
    gNIntensity = 1;

    // randomly define the length of the next trail
    mTrailLength = random(1,5);

  }

}

